import sys
previous_coreuserid = None
previous_levelid = None
attemptNumber = 0
for line in sys.stdin:
    coreuserid, dt, flavourid, levelid, gamestarttimestamp, msts = line.rstrip().split('\t')
    if levelid != previous_levelid:
        attemptNumber = 1
    elif coreuserid != previous_coreuserid:
        attemptNumber = 1
    else:
        attemptNumber += 1
    previous_coreuserid = coreuserid
    previous_levelid = levelid
    print '%s\t%s\t%s\t%s\t%s\t%s\t%s' % (coreuserid, dt, flavourid, levelid, gamestarttimestamp, msts, attemptNumber)